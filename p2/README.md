

# LIS4368 - Advanced Web Application Development

## Sarah Huerta

#### Project 2 Requirements:

1. Use JSP/Servlets web Application to implement CRUD functionality.
2. Introduction of SCRUD option to search data.
3. Associated Screenshots Deliverables`



#### Data Entry with Valid User Form Entry

| Before | After |
| ------- | ------ |
| ![1](insertb4.png) | ![2](insertaf.png) |

#### Displaying Data

| Displaying Data |
| ------- |
| ![3](webtab.png) |


#### Modifying Form

| Before Modifying | After |
| ------- | ------ |
| ![1](mod.png) | ![2](moding.png) |

#### Deleting Data

| Deleting Data |
| ------- |
| ![3](del.png) |

#### Database Changes (Select, Insert, Update, Delete)

| SQL Tables |
| ------- |
| ![3](sql.png) | 
